The "An Untitled Story" on ENIGMA project.

This repository contains the "An Untitled Story" original source files from:
http://www.mattmakesgames.com/

However, they are saved in the ENIGMA project file format. 
However x2, the ENIGMA zip has been expanded to allow for easier revision control.

The easiest way to get up and running is as follows:
  1) Install ENIGMA: http://enigma-dev.org/
  2) Select *everything* in the aus_enigma directory and zip it into a single file (using "zip" compression) with the extension .egm.
  3) Open this file in ENIGMA, but make *sure* you do it from the aus_enigma directory (since it needs access to the "BGs" folder).

Optionally:
  1) You can remove the "music" folder from the egm zip to save space.
  2) You can scour the web for the game's fonts and install them (we don't serialize fonts... yet).

Please see license.txt for the original license, and some important notes relating to distribution.
Note:
  1) I have clearly marked the game as an "unofficial ENIGMA port" in the title screen.
  2) In the intro, I have clearly noted that I (Seth N. Hetu) made modifications for ENIGMA.

